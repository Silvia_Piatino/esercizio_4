ORD_PARTENZA = 999

class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str, ingredients):
        self.Name = name
        self.listaIngredientiPizza = ingredients

    def addIngredient(self, ingredient: Ingredient):
        if self.listaIngredientiPizza.count(ingredient)>2:
            raise ValueError("Ingredient already inserted")
        self.listaIngredientiPizza.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.listaIngredientiPizza)

    def computePrice(self) -> int:
        return sum([i.Price for i in self.listaIngredientiPizza])


class Order:
    def __init__(self, pizzas):
        self.listaPizzeOrdinate = pizzas

    def getPizza(self, position: int) -> Pizza:
        if position<1 or position > self.numPizzas():
            raise ValueError("Position passed is wrong")
        return self.listaPizzeOrdinate[position-1]

    def initializeOrder(self, numPizzas: int):
        pass

    def addPizza(self, pizza: Pizza):
        self.listaPizzeOrdinate.append(pizza)

    def numPizzas(self) -> int:
        return len(self.listaPizzeOrdinate)

    def computeTotal(self) -> int:
        return sum([i.computePrice() for i in self.listaPizzeOrdinate])


class Pizzeria:
    def __init__(self):
        self.listaIngredientiPizzeria=[]
        self.listaPizzePizzeria=[]
        self.listaOrdiniPizzeria=[]

    def addIngredient(self, name: str, description: str, price: int):
        ingrediente= Ingredient(name, price, description)
        if ingrediente.Name in [i.Name for i in self.listaIngredientiPizzeria]:
            raise ValueError("Ingredient already inserted")
        self.listaIngredientiPizzeria.append(ingrediente)

    def findIngredient(self, name: str) -> Ingredient:
        for i in self.listaIngredientiPizzeria:
            if i.Name is name:
                return i
        raise ValueError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        if name in [i.Name for i in self.listaPizzePizzeria]:
            raise ValueError("Pizza already inserted")
        # for i in ingredients:
        # pizzaIngredients.append(self.findIngredient(i))
        pizzaIngredients = [self.findIngredient(i) for i in ingredients]
        nuovaPizza = Pizza(name, pizzaIngredients)  # sto creando nuovaPizza chiamando il costruttore della classe pizza
        self.listaPizzePizzeria.append(nuovaPizza)

    def findPizza(self, name: str) -> Pizza:
        for i in self.listaPizzePizzeria:
            if i.Name is name:
                return i
        raise ValueError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise ValueError("Empty order")
        nuovoOrdine = Order([self.findPizza(i) for i in pizzas])
        self.listaOrdiniPizzeria.append(nuovoOrdine)
        numOrder = len(self.listaOrdiniPizzeria)+ORD_PARTENZA
        return numOrder

    def findOrder(self, numOrder: int) -> Order:
        numOrderMax = len(self.listaOrdiniPizzeria)+ORD_PARTENZA
        if numOrder<=ORD_PARTENZA or numOrder>numOrderMax:
            raise ValueError("Order not found")
        return self.listaOrdiniPizzeria[numOrder-ORD_PARTENZA-1]

    def getReceipt(self, numOrder: int) -> str:
        ordineCorrente = self.findOrder(numOrder)
        # totale = sum([i.computePrice() for i in  [ordineCorrente.getPizza(j+1) for j in range(ordineCorrente.numPizzas())]]) # doppia comprehension
        totale = sum([i.computePrice() for i in ordineCorrente.listaPizzeOrdinate])
        receipt = "\n".join(["- " + i.Name + ", " + str(i.computePrice()) + " euro" for i in ordineCorrente.listaPizzeOrdinate])
        receipt+="\n  TOTAL: " + str(totale) + " euro\n"
        return receipt

    def listIngredients(self) -> str:
        ingredientiLista = sorted(self.listaIngredientiPizzeria, key = lambda x: x.Name)
        list = "\n".join([i.Name+" - '"+ i.Description + "': " +str(i.Price)+" euro" for i in ingredientiLista])
        list+="\n"
        return list

    def menu(self) -> str:
        menu="\n".join([i.Name+" (" +str(i.numIngredients()) +" ingredients): " +str(i.computePrice()) +" euro" for i in self.listaPizzePizzeria])
        menu+="\n"
        return menu