#include<algorithm>
#include "Pizzeria.h"
#define ORD_PARTENZA 999

namespace PizzeriaLibrary {
    Ingredient::Ingredient(const string& name, const string& description, const int& price){
        Name = name;
        Description = description;
        Price = price;
    }
    Pizza::Pizza(const string& name, const vector<Ingredient> ingredients){
        Name = name;
        listaIngredientiPizza = ingredients;
        /*unsigned int numElements = ingredients.size();
        for(unsigned int i=0; i<numElements; i++)
            listaIngredientiPizza.push_back(ingredients[i]);*/
    }
    Order::Order(const vector<Pizza> pizzas){
        listaPizzeOrdinate = pizzas;
        /*unsigned int numElements = pizzas.size();
        for(unsigned int i=0; i<numElements; i++)
            listaPizzeOrdinate.push_back(pizzas[i]);*/
    }
    void Order::InitializeOrder(int numPizzas){
        listaPizzeOrdinate.reserve(numPizzas); //resize non va bene perché cerca di riempire la nuova capacità chiamando il default constructor di pizza, che non c'è
    }
    void Order::AddPizza(const Pizza& pizza){
        listaPizzeOrdinate.push_back(pizza);
    }
    const Pizza& Order::GetPizza(const int& position) const{
        const int numElements = NumPizzas();
        if( position<1 or position>numElements)
            throw runtime_error("Position passed is wrong");
        return listaPizzeOrdinate[position-1];
    }
    int Order::ComputeTotal() const{
        unsigned int numElements = NumPizzas();
        int sum=0;
        for(unsigned int i=0; i<numElements; i++){
            sum+=listaPizzeOrdinate[i].ComputePrice();
        }
        return sum;
    }
    void Pizza::AddIngredient(const Ingredient& ingredient) {
        unsigned int numElements= NumIngredients();
        unsigned int q=0;
        for(unsigned int i=0; i<numElements; i++){ //controllo che l'ingrediente non venga inserito più di 2 volte
            if(ingredient.Name==listaIngredientiPizza[i].Name)
                q++;
            if(q>2)
                throw runtime_error("Ingredient already inserted");
        }
        listaIngredientiPizza.push_back(ingredient);
    }
    int Pizza::ComputePrice() const{
        unsigned int numElements= NumIngredients();
        int sum=0;
        for(unsigned int i=0; i<numElements; i++){
            sum+=listaIngredientiPizza[i].Price;
        }
        return sum;
    }
    void Pizzeria::AddIngredient(const string& name, const string& description, const int& price)
    {
        Ingredient ingrediente(name, description, price);
        unsigned int numElements= listaIngredientiPizzeria.size();
        for(unsigned int i=0;i<numElements; i++){
            if(listaIngredientiPizzeria[i].Name==ingrediente.Name)
                throw runtime_error("Ingredient already inserted");
        }
        listaIngredientiPizzeria.push_back(ingrediente);
    }
    const Ingredient& Pizzeria::FindIngredient(const string& name)const{ // const string& passo per referenza un parametro non modificabile
        unsigned int numElements= listaIngredientiPizzeria.size();
        for(unsigned int i=0;i<numElements; i++){
            if(listaIngredientiPizzeria[i].Name==name)
                return listaIngredientiPizzeria[i];
        }
        throw runtime_error("Ingredient not found");
    }
    void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients){
        vector<Ingredient> pizzaIngredients;
        unsigned int numElements = ingredients.size(); //numero ingredienti che metto nella pizza
        unsigned int numPizzasPizzeria = listaPizzePizzeria.size(); //numero di pizze disponibili nella pizzeria
        for(unsigned int i=0; i<numPizzasPizzeria; i++){
            if(listaPizzePizzeria[i].Name==name)
                throw runtime_error("Pizza already inserted");
        }
        for(unsigned int i=0; i<numElements; i++)
            pizzaIngredients.push_back(FindIngredient(ingredients[i]));
        Pizza nuovaPizza(name, pizzaIngredients);
        listaPizzePizzeria.push_back(nuovaPizza);
    }
    const Pizza& Pizzeria::FindPizza(const string& name)const{
        unsigned int numPizzasPizzeria = listaPizzePizzeria.size();
        for(unsigned int i=0; i<numPizzasPizzeria; i++){
            if(listaPizzePizzeria[i].Name==name)
                return listaPizzePizzeria[i];
        }
        throw runtime_error("Pizza not found");
    }
    int Pizzeria::CreateOrder(const vector<string>& pizzas){
        vector<Pizza> orderedPizzas;
        int numOrder=0;
        unsigned int numElements = pizzas.size();
        if(numElements==0)
            throw runtime_error("Empty order");
        for(unsigned int i=0; i<numElements; i++)
            orderedPizzas.push_back(FindPizza(pizzas[i]));
        Order nuovoOrdine(orderedPizzas);
        listaOrdiniPizzeria.push_back(nuovoOrdine);
        numOrder=listaOrdiniPizzeria.size()+ORD_PARTENZA;
        return numOrder;
    }
    const Order& Pizzeria::FindOrder(const int& numOrder) const{
        const int numOrderMax=listaOrdiniPizzeria.size()+ORD_PARTENZA;
        if(numOrder<=ORD_PARTENZA or numOrder>numOrderMax)
            throw runtime_error("Order not found");
        return listaOrdiniPizzeria[numOrder-ORD_PARTENZA-1];
    }
    string Pizzeria::GetReceipt(const int& numOrder) const{
        Order ordineCorrente(FindOrder(numOrder)); // su questo oggetto non posso chiamare begin e end perché è una classe che contiene un vettore, non è un vettore; per usarli dovrei definirli nella classe come metodi
        unsigned int numPizzas = ordineCorrente.NumPizzas();
        int sum=0;
        string receipt;
        vector<Pizza> pizzeOrdinate;
        for(unsigned int i=1; i<=numPizzas; i++){
            sum+=ordineCorrente.GetPizza(i).ComputePrice();
            pizzeOrdinate.push_back(ordineCorrente.GetPizza(i));//pizze ancora disordinate
        }
        for(unsigned int i=0; i<numPizzas; i++)
            receipt.append("- " + pizzeOrdinate[i].Name + ", " + to_string(pizzeOrdinate[i].ComputePrice()) + " euro" + "\n");
        receipt.append("  TOTAL: " + to_string(sum) + " euro" + "\n");
        return receipt;
    }
    string Pizzeria::ListIngredients() const{
        vector<Ingredient> ingredientiLista;
        unsigned int numIngredients = listaIngredientiPizzeria.size();
        string list;
        for(unsigned int i=0; i<numIngredients; i++){
            ingredientiLista.push_back(listaIngredientiPizzeria[i]);
        }
        sort(ingredientiLista.begin(), ingredientiLista.end(), [](const Ingredient& lhs, const Ingredient& rhs){
            return lhs.Name < rhs.Name;
        });
        for(unsigned int i=0; i<numIngredients; i++){
            list.append(ingredientiLista[i].Name + " - '" + ingredientiLista[i].Description + "': " + to_string(ingredientiLista[i].Price) + " euro" + "\n");
        }
        return list;
    }
    string Pizzeria::Menu() const{
        unsigned int numPizze= listaPizzePizzeria.size();
        string menu;
        for(int i=numPizze-1; i>=0; i--){ //IMPORTANTE: occhio che unsigned int non può mai andare in negativo. Se vuoi reversare usa int, non unsigned int
            menu.append(listaPizzePizzeria[i].Name + " (" + to_string(listaPizzePizzeria[i].NumIngredients()) + " ingredients): " + to_string(listaPizzePizzeria[i].ComputePrice()) + " euro" + "\n");
        }

        return menu;
    }
}
