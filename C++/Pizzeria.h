#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
      Ingredient(const string& name, const string& description, const int& price);
  };

  class Pizza {
    private:
      vector<Ingredient> listaIngredientiPizza;
    public:
      string Name;
      Pizza(const string& name, const vector<Ingredient> ingredients);

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const { return listaIngredientiPizza.size(); }
      int ComputePrice() const;
  };

  class Order {
    private:
      vector<Pizza> listaPizzeOrdinate;
    public:
      Order(const vector<Pizza> pizzas);

      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const{ return listaPizzeOrdinate.size();} //getter
      int ComputeTotal() const;
  };

  class Pizzeria {
    private:
      vector<Ingredient> listaIngredientiPizzeria;
      vector<Pizza> listaPizzePizzeria;
      vector<Order> listaOrdiniPizzeria;
    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name)const; //il secondo const serve a non modificare gli attributi di pizzeria
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
